<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('masp')->unique();
            $table->string('chatlieu');
            $table->string('mausac');
            $table->string('kichco');
            $table->integer('gia');
            $table->string('tinhtrang');
            $table->integer('manufacturer_id')->unsigned();

            $table->integer('assortment_id')->unsigned();

            $table->timestamps();
        });
        Schema::table('products',function (Blueprint $table){
            $table->foreign('manufacturer_id')->references('id')->on('manufacturer')->onDelete('cascade');
            $table->foreign('assortment_id')->references('id')->on('assortment')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
